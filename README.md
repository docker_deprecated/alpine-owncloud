# alpine-owncloud

#### [alpine-x64-owncloud](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-owncloud/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-owncloud.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-owncloud "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-owncloud.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-owncloud "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-owncloud.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-owncloud/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-owncloud.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-owncloud/)
#### [alpine-aarch64-owncloud](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-owncloud/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64/alpine-aarch64-owncloud.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-owncloud "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64/alpine-aarch64-owncloud.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-owncloud "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-owncloud.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-owncloud/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-owncloud.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-owncloud/)
#### [alpine-armhf-owncloud](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-owncloud/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-owncloud.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-owncloud "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-owncloud.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-owncloud "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-owncloud.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-owncloud/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-owncloud.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-owncloud/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [ownCloud](https://owncloud.org/)
    - ownCloud is an open source, self-hosted file sync and share app platform.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -e RUN_USER_NAME=<run_user_name> \
           -p 80:80/tcp \
           forumi0721alpinex64/alpine-x64-owncloud:latest
```

* aarch64
```sh
docker run -d \
           -e RUN_USER_NAME=<run_user_name> \
           -p 80:80/tcp \
           forumi0721alpineaarch64/alpine-aarch64-owncloud:latest
```

* armhf
```sh
docker run -d \
           -e RUN_USER_NAME=<run_user_name> \
           -p 80:80/tcp \
           forumi0721alpinearmhf/alpine-armhf-owncloud:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:80/](http://localhost:80/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 80/tcp             | HTTP port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /data              | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| RUN_USER_NAME      | Run user name                                    |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-owncloud](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-owncloud/)
* [forumi0721alpineaarch64/alpine-aarch64-owncloud](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-owncloud/)
* [forumi0721alpinearmhf/alpine-armhf-owncloud](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-owncloud/)

